package vendingMachine;
//java enum to represent coins and bills supported by vending machine
public enum Coins {
    PENNY(1), 
    NICKLE(5), 
    DIME(10), 
    QUARTER(25),
    HALF(50),
	oneDollar(100),
	twoDollar(200);
	
	private int denomination; 
	
	private Coins(int denomination){ 
		this.denomination = denomination; 
		} 
	
	public int getDenomination(){ 
		return denomination; 
		}



}
