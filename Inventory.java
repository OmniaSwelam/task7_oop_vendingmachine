package vendingMachine;

import java.util.HashMap;

//represent items inventory inside vending machine
//T can be a product, coin or bill
public class Inventory<T>{
	
	private HashMap<T, Integer> inventory= new HashMap<T, Integer>();
	
	//increase number of pieces of that item by one
	public void addItem (T t) {
		int count= inventory.get(t);
		inventory.put(t, count+1);
	}
	
	public void subtractItem (T t) {
		if (hasItem(t)) {
			int count= inventory.get(t);
			inventory.put(t, count-1);
		}
	}
	
	//function view available quantity for each item
	public int getQuantity (T t) {
		int count= inventory.get(t);
		return count;
	}
	
	public boolean hasItem (T t) {
		return getQuantity(t) >0 ;
		
	}
	
	//put specific amount/quantity for each item
	public void put (T t, int quantity) {
		inventory.put(t, quantity);
	}
	
	//Loop over hashmap to print all contents available in inventory
	public void displayInventory() {
		inventory.forEach((k,v)->System.out.println("Item : " + k + " quantity : " + v));

		}
}
