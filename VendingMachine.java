package vendingMachine;

import java.util.ArrayList;


public class VendingMachine {
	//Inventory of vending machine
	private Inventory<Item> ItemInventory ;
	private Inventory<Coins> CashInventory;
	
	private int totalCost=0; //total cost of items that customer want to buy
	private int changeAmount=0;
	private int currentBalance=0 ; //for customer, money that customer puts in machine in cents
	private ArrayList<Item> currentItemsPerTransaction; //Items that customer will select to buy
	protected ArrayList<Item> dispersedItems; //returned items for customer after buying
	private ArrayList<Coins> changes; //list of coins that will return to user for his change
	
	public VendingMachine() {
		currentItemsPerTransaction= new ArrayList<Item>();
		dispersedItems= new ArrayList<Item>();
		changes= new ArrayList<Coins>();
		initialize();
	}
	//---------------------------------------------------------------------------------
	private void initialize() {
		//put 5 pieces for each of Items/products , coins and bills
		for (Item i: Item.values()) {
			ItemInventory.put(i, 5);
		}
		for (Coins c: Coins.values()) {
			CashInventory.put(c, 5);
		}

	}
	//---------------------------------------------------------------------------------
	//allow the user to put money he wants in machine, before selecting items to buy
	public void insertMoney(Coins c) {
		currentBalance+= c.getDenomination();
		CashInventory.addItem(c);
		}
	
	//---------------------------------------------------------------------------------
	//to select item to buy, add it to items bucket to buy in same transaction and inform customer its price
	public int selectItemAndGetPrice(Item t) {
		if(ItemInventory.hasItem(t)) {
			currentItemsPerTransaction.add(t);
			return  t.getPrice();
		}
		else
			throw new SoldOutException("Sold Out, Please buy another item ");
		
	}
	//--------------------------------------------------------------------------------
	public int calculateTotalCost() {
		for(int i=0; i<currentItemsPerTransaction.size();i++) {
			totalCost+= currentItemsPerTransaction.get(i).getPrice();
		}
		return totalCost;
	}
	//--------------------------------------------------------------------------------
	public boolean isFullPaid() {

		if (currentBalance>= calculateTotalCost()) {
			return true;
		}
		else
			return false;
	}
	//----------------------------------------------------------------------------------
	//to disperse selected items: must paid and has sufficient change
	public ArrayList<Item> collectItems() {
		if(isFullPaid()) {
			if(hasSufficientChange()) {
				for(int i=0; i<currentItemsPerTransaction.size(); i++) {
					ItemInventory.subtractItem(currentItemsPerTransaction.get(i));
					dispersedItems.add(currentItemsPerTransaction.get(i));
				}
				collectChange();
				Bill.printBill(dispersedItems, totalCost, currentBalance,changeAmount);
				Reset();
				return dispersedItems;
			}
			else
				throw new NoChangeAvailableException("Not Sufficient change in Inventory");
		}
		int remainingBalance= totalCost- currentBalance;
		throw new NotSufficientPaidException("Price is not full paid, remaining: ", remainingBalance);
	}
	//----------------------------------------------------------------------------------
	private boolean hasSufficientChangeForAmount(int amount) {
		boolean hasSufficientChange=true;
		try {
			getChange(amount);
		}
		catch(NoChangeAvailableException e) {
			hasSufficientChange=false;
		}
		return hasSufficientChange;
	}
	//-----------------------------------------------------------------------------------
	private boolean hasSufficientChange() {
		return hasSufficientChangeForAmount(currentBalance- totalCost);
	}
	//-----------------------------------------------------------------------------------
	//return certain amount of change to customer
	private ArrayList<Coins> getChange(int amount) {
		if (amount>0) {
			int balance= amount;
			while(balance>0) {
				if(balance >= Coins.twoDollar.getDenomination() && CashInventory.hasItem(Coins.twoDollar)){ 
					changes.add(Coins.twoDollar); 
					CashInventory.subtractItem(Coins.twoDollar);
					balance = balance - Coins.twoDollar.getDenomination(); 
					continue;
				}
				else if(balance >= Coins.oneDollar.getDenomination() && CashInventory.hasItem(Coins.oneDollar)){ 
					changes.add(Coins.oneDollar); 
					CashInventory.subtractItem(Coins.oneDollar);
					balance = balance - Coins.oneDollar.getDenomination(); 
					continue;
				}
			    else if(balance >= Coins.HALF.getDenomination() && CashInventory.hasItem(Coins.HALF)){ 
					changes.add(Coins.HALF); 
					CashInventory.subtractItem(Coins.HALF);
					balance = balance - Coins.HALF.getDenomination(); 
					continue;
				}
				else if(balance >= Coins.QUARTER.getDenomination() && CashInventory.hasItem(Coins.QUARTER)){
					changes.add(Coins.QUARTER); 
					CashInventory.subtractItem(Coins.QUARTER);
					balance = balance - Coins.QUARTER.getDenomination(); 
					continue;
				}
				else if(balance >= Coins.DIME.getDenomination() && CashInventory.hasItem(Coins.DIME)){
					changes.add(Coins.DIME); 
					CashInventory.subtractItem(Coins.DIME);
					balance = balance - Coins.DIME.getDenomination(); 
					continue;
				}
				else if(balance >= Coins.NICKLE.getDenomination() && CashInventory.hasItem(Coins.NICKLE)){
					changes.add(Coins.NICKLE); 
					CashInventory.subtractItem(Coins.NICKLE);
					balance = balance - Coins.NICKLE.getDenomination(); 
					continue;
				}
				else if(balance >= Coins.PENNY.getDenomination() && CashInventory.hasItem(Coins.PENNY)){
					changes.add(Coins.PENNY); 
					CashInventory.subtractItem(Coins.PENNY);
					balance = balance - Coins.PENNY.getDenomination(); 
					continue;
				}				

			}
		}
		
		else //amount<0
			throw new NoChangeAvailableException("NotSufficientChange, Please try another product");
		
		return changes;
			
		}
	
	//---------------------------------------------------------------------------------------------------
	//calculate change and returns it, make current balance=0
	private ArrayList<Coins> collectChange(){
		changeAmount= currentBalance- totalCost;
		ArrayList<Coins>change = getChange(changeAmount);
		return change;
	}
	//---------------------------------------------------------------------------------------------------
	public void Reset() {
		currentBalance=0;
		currentBalance=0;
		currentItemsPerTransaction=null;
		changes=null;
	}
	
	//---------------------------------------------------------------------------------------------------
	public ArrayList<Coins> refund() {
		ArrayList<Coins> refund= getChange(currentBalance);
		currentBalance= 0;
		return refund;
	}
	//---------------------------------------------------------------------------------------------------
	//display all items in ItemInventory with their quantities
	public void queryVendingMachine() {
		ItemInventory.displayInventory();
	}
	
	
	
}
