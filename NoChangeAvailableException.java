package vendingMachine;
//Vending Machine throws this exception to indicate that it doesn't have change to complete this request.

public class NoChangeAvailableException extends RuntimeException{
	
	private String message;
	
	public NoChangeAvailableException (String message) {
		this.message= message;
	}
	
	@Override
	public String getMessage() {
		return this.message;
	}
	
}
