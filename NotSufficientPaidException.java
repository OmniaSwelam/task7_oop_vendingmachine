package vendingMachine;
//An Exception, thrown by Vending Machine when a user tries to collect an item, without paying the full amount.

public class NotSufficientPaidException extends RuntimeException {
	
	private String message; 
	private int remaining; 
	
	public NotSufficientPaidException(String message, int remaining) { 
		this.message = message; 
		this.remaining = remaining; 
		} 
	
	public int getRemaining(){ 
		return remaining; 
		}
	
	@Override
	public String getMessage() {
		return message + remaining;
	}


}
