package vendingMachine;

import java.util.ArrayList;
import java.util.Collections;

//java class to represent products sold to customer and their prices

public class Bill {
	public static String printBill(ArrayList<Item> soldItems, int totalCost, int totalPaid, int changeAmount) {
	    StringBuilder sb = new StringBuilder();
	    sb.append("Sold Items	"+ "	\n");
	    sb.append("        --------------------     "+"\n");
	    sb.append("\n");
	    
	    for (int i=0; i<soldItems.size(); i++) {
	    	sb.append(soldItems.get(i).getName()+ String.join("", Collections.nCopies(30- soldItems.get(i).getName().length(), " "))+soldItems.get(i).getPrice());
	    }
	    
	    sb.append("\n");
	    sb.append("Total Cost: "+ totalCost+"\n" );
	    sb.append("Total Paid: "+ totalPaid+"\n" );
	    sb.append("Remaining Change: "+ changeAmount+"\n" );
	    
	    return sb.toString();
	}

}

